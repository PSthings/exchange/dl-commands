##### **Basic powershell commands to modify Distribution Groups**
**`Note: All Distribution lists related commands need to be run to on-prem exchange console in a hybrid environment and will sync to O365 in about 30 mins`**

--- 
<br>

**This list is divided into groups below for easy navigation:**
1. **New** command (create new DL)
2. **Get** commands (Pull or check information about DL)
3. **Set** commands (modify existing properties of DL)
4. **Add** commands (ADD new members/items to DL)
5. **Remove** commands (Remove items from DL)

`Replace`**`"enter_emailaddress_here"`**` and other quoted items with actual exchange objects as needed for all commands below`

--- 
<br>

| Create NEW DL |
| ------ |

**Create new Distribution list**
```powershell
New-DistributionGroup -Name "name_of_DL" -Alias "primary_alias" -DisplayName "display_name_of_DL" -ManagedBy "owner_username" -Members member1,member2,

# above is one liner but you can define these values separately later. 
# -Name parameter is required.
```
<br>

| GET commands |
| ------ |

**Get Name, DisplayName and PrimarSMTPaddress of DL**
```powershell
Get-DistributionGroup -Identity "enter_emailaddress_here"
```
**Get total count of members in a DL**
```powershell
Get-DistributionGroup -Identity "enter_emailaddress_here" -ResultSize Unlimited | select DisplayName,@{n="MemberCount";e={(Get-DistributionGroupMember $_.PrimarySmtpAddress -ResultSize Unlimited | measure).Count}}
```

**Get owner of a DL**
```powershell
(Get-DistributionGroup "enter_emailaddress_here").managedby | foreach {$_.tostring().split("/")[-1]}
```
<br>

| SET commands |
| ------ |

**Change Display name of distribution group**
```powershell
Set-DistributionGroup -Identity "enter_emailaddress_here" -Displayname "newdisplayname"
```

**Add and Set new Alias** 
```powershell
Set-DistributionGroup -Identity "enter_emailaddress_here" -EmailAddresses @{add='newalias@domain.com'}
# Now to set the newly added alias as primarysmptpaddress
Set-DistributionGroup "enter_emailaddress_here" -Alias "newalias@domain.com"
```

**Enable Moderation of DL**
```powershell
Set-DistributionGroup "enter_emailaddress_here" -ModerationEnabled $true -ModeratedBy "user1","user2" SendModerationNotifications Internal
```

**Allow external senders be able to send to DL**
```powershell
Set-DistributionGroup "enter_emailaddress_here" -RequireSenderAuthenticationEnabled $False
```
<br>

| ADD commands |
| ------ |

**Add an additional member to DL**
```powershell
Add-DistributionGroupMember -Identity "enter_emailaddress_here" -Member "ADusername"
```

**Add multiple members to a DL**
```powershell
# First create a csv file listing all the usernames that needs to be added. name the header of columns "user"
Import-CSV "C:\pathto\csvfile.csv" | Foreach{Add-DistributionGroupMember -Identity "enter_emailaddress_here" -Member $_.user}
```
<br>

| REMOVE commands |
| ------ |

**Remove a single member from DL**
```powershell
Remove-DistributionGroupMember -Identity "enter_emailaddress_here" -Member "ADusername"
```

**Remove multiple members from a DL**
```powershell
# First create a csv file listing all the usernames that needs to be added. name the header of columns "user"
Import-CSV "C:\pathto\csvfile.csv" | Foreach{Remove-DistributionGroupMember -Identity "enter_emailaddress_here" -Member $_.user}
```

**Remove ALL members from a DL**
```powershell
Set-ADgroup -identity group_name -Clear member
```
<br>

| DELETE Distribution List |
| ------ |
```powershell
# use with caution as this will delete whole group 
Remove-DistributionGroup -Identity "DL_group_name"
```
